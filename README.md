Documentation site on Heroku
=====================================

A minimal webapp to serve documentation written in markdown.  
Documentation kept in vcs, along with the project it pertains to, if any.  

## Implementation
Client asks for ´/doc/page.md´: respond with ´/doc/page.md´  
Client asks for ´/doc/page.html´: respond with an html page that render page.md inside.  
Rendering is done using JavaScript MarkDown renderer [PageDown][pd], [Marked][md] or [ShowDown][sd] and [google-code-prettify][prettify] syntax highlighting  
Server side logic is done with [Play 2][play] [Scala][scala]

## Infrastructure

[Play2 on Heroku][p2h]  
[Heroku Scala Play2 Sample][p2hsample]  

heroku create  
git push heroku master  
play debug run  

(in fork) git pull upstream master  


[pd]: http://code.google.com/p/pagedown/wiki/PageDown
[md]: https://github.com/chjj/marked
[sd]: https://github.com/coreyti/showdown
[prettify]: http://code.google.com/p/google-code-prettify/
[play]: http://www.playframework.org/
[scala]: http://www.scala-lang.org/

[p2h]: https://github.com/playframework/Play20/wiki/ProductionHeroku
[p2hsample]: https://github.com/heroku/scala-play-sample

